import gameObjects.EnterpriseShip;
import gameObjects.KlingonShip;
import gameObjects.StarBase;
import map.Galaxy;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class EnterpriseShipTests {

    @Test
    public void shouldRefillEnergyWhileBeingNextToStarBase() {
        Galaxy galaxy = new Galaxy(8, 8);
        EnterpriseShip enterpriseShip = new EnterpriseShip(galaxy);
        enterpriseShip.setEnergy(100);
        enterpriseShip.placeInQuadrant(0, 1);
        enterpriseShip.placeInSector(0, 0);
        StarBase starBase = new StarBase(galaxy);
        starBase.placeInQuadrant(0, 1);
        starBase.placeInSector(0, 1);

        enterpriseShip.consumeEnergy(20);
        enterpriseShip.tryRefillEnergy();

        assertEquals(100, (int) enterpriseShip.getEnergy());
    }

    @Test
    public void shouldNotRefillEnergyWhileBeingFarFromStarBase() {
        Galaxy galaxy = new Galaxy(8, 8);
        EnterpriseShip enterpriseShip = new EnterpriseShip(galaxy);
        enterpriseShip.setEnergy(100);
        enterpriseShip.placeInQuadrant(0, 1);
        enterpriseShip.placeInSector(0, 0);
        StarBase starBase = new StarBase(galaxy);
        starBase.placeInQuadrant(0, 1);
        starBase.placeInSector(0, 3);

        enterpriseShip.consumeEnergy(20);
        enterpriseShip.tryRefillEnergy();

        assertEquals(80, (int) enterpriseShip.getEnergy());
    }

    @Test
    public void shouldReturnNoTargets() {
        Galaxy galaxy = new Galaxy(8, 8);
        EnterpriseShip enterpriseShip = new EnterpriseShip(galaxy);
        enterpriseShip.setEnergy(100);
        enterpriseShip.placeInQuadrant(0, 0);
        enterpriseShip.placeInSector(0, 0);

        KlingonShip klingonShip = enterpriseShip.findTarget(KlingonShip.class);

        assertNull(klingonShip);
    }

    @Test
    public void shouldReturnClosestTarget() {
        Galaxy galaxy = new Galaxy(8, 8);
        EnterpriseShip enterpriseShip = new EnterpriseShip(galaxy);
        enterpriseShip.setEnergy(100);
        enterpriseShip.placeInQuadrant(0, 0);
        enterpriseShip.placeInSector(0, 0);
        KlingonShip klingonShip = new KlingonShip(galaxy);
        klingonShip.placeInQuadrant(0, 0);
        klingonShip.placeInSector(0, 3);
        KlingonShip klingonShip2 = new KlingonShip(galaxy);
        klingonShip2.placeInQuadrant(0, 0);
        klingonShip2.placeInSector(0, 4);

        KlingonShip target = enterpriseShip.findTarget(KlingonShip.class);

        assertEquals(klingonShip, target);
    }
}
