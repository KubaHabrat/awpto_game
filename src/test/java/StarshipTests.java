import gameObjects.EnterpriseShip;
import gameObjects.Star;
import gameObjects.Starship;
import map.Galaxy;
import map.Quadrant;
import map.Sector;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StarshipTests {

    Galaxy galaxy;
    Starship starship;

    @Before
    public void initEnvironment() {
        galaxy = new Galaxy(8, 8);
        starship = new Starship(galaxy);
    }

    @Test
    public void shouldReturnCorrectQuadrant() {
        starship.placeInQuadrant(0, 1);

        Quadrant quadrant = starship.getCurrentQuadrant();

        assertEquals(0, quadrant.getX());
        assertEquals(1, quadrant.getY());
    }

    @Test
    public void shouldReturnCorrectSector() {
        starship.placeInQuadrant(0, 0);

        starship.placeInSector(1, 2);
        Sector sector = starship.getCurrentSector();

        assertEquals(1, sector.getX());
        assertEquals(2, sector.getY());
    }

    @Test
    public void shouldConsumeEnergy() {
        starship.setEnergy(100);

        starship.consumeEnergy(20);

        assertEquals(80, (int) starship.getEnergy());
    }

    @Test
    public void shouldConsumeEnergyAfterMove() {
        starship.placeInQuadrant(0, 0);
        starship.placeInSector(1, 1);
        starship.setEnergy(100);

        starship.move(4, 0);

        assertEquals(96, (int) starship.getEnergy());
    }

    @Test
    public void shouldMoveToRightSector() {
        starship.placeInQuadrant(0, 0);
        starship.placeInSector(1, 1);
        starship.setEnergy(100);

        starship.move(1, 0);
        Sector sector = starship.getCurrentSector();

        assertEquals(2, sector.getX());
        assertEquals(1, sector.getY());
    }

    @Test
    public void shouldMoveToLeftSector() {
        starship.placeInQuadrant(0, 0);
        starship.placeInSector(1, 1);
        starship.setEnergy(100);

        starship.move(-1, 0);
        Sector sector = starship.getCurrentSector();

        assertEquals(0, sector.getX());
        assertEquals(1, sector.getY());
    }

    @Test
    public void shouldMoveToLowerSector() {
        starship.placeInQuadrant(0, 0);
        starship.placeInSector(1, 1);
        starship.setEnergy(100);

        starship.move(0, -1);
        Sector sector = starship.getCurrentSector();

        assertEquals(1, sector.getX());
        assertEquals(0, sector.getY());
    }

    @Test
    public void shouldMoveToUpperSector() {
        starship.placeInQuadrant(1, 1);
        starship.placeInSector(1, 1);
        starship.setEnergy(100);

        starship.move(0, 1);
        Sector sector = starship.getCurrentSector();

        assertEquals(1, sector.getX());
        assertEquals(2, sector.getY());
    }

    @Test
    public void shouldMoveToRightQuadrant() {
        starship.placeInQuadrant(1, 1);
        starship.placeInSector(1, 1);
        starship.setEnergy(100);

        starship.move(8, 0);
        Quadrant quadrant = starship.getCurrentQuadrant();

        assertEquals(2, quadrant.getX());
        assertEquals(1, quadrant.getY());
    }

    @Test
    public void shouldMoveToLeftQuadrant() {
        starship.placeInQuadrant(1, 1);
        starship.placeInSector(1, 1);
        starship.setEnergy(100);

        starship.move(-8, 0);
        Quadrant quadrant = starship.getCurrentQuadrant();

        assertEquals(0, quadrant.getX());
        assertEquals(1, quadrant.getY());
    }

    @Test
    public void shouldMoveToLowerQuadrant() {
        starship.placeInQuadrant(1, 1);
        starship.placeInSector(1, 1);
        starship.setEnergy(100);

        starship.move(0, -8);
        Quadrant quadrant = starship.getCurrentQuadrant();

        assertEquals(1, quadrant.getX());
        assertEquals(0, quadrant.getY());
    }

    @Test
    public void shouldMoveToUpperQuadrant() {
        starship.placeInQuadrant(1, 1);
        starship.placeInSector(1, 1);
        starship.setEnergy(100);

        starship.move(0, 8);
        Quadrant quadrant = starship.getCurrentQuadrant();

        assertEquals(1, quadrant.getX());
        assertEquals(2, quadrant.getY());
    }

    @Test
    public void shouldConsumeStarDateOnQuadrantChange() {
        starship.placeInQuadrant(0, 0);
        starship.placeInSector(4, 4);
        starship.setStarDates(60);

        starship.move(8, 0);

        assertEquals(59, (int) starship.getStarDates());
    }

    @Test
    public void shouldConsumeEnergyWhileAttackAndAbsorb() {
        starship.setEnergy(100);
        starship.placeInQuadrant(0, 0);
        starship.placeInSector(4, 4);
        Starship targetStarship = new Starship(galaxy);
        targetStarship.setEnergy(100);
        targetStarship.placeInQuadrant(0, 0);
        targetStarship.placeInSector(4, 5);

        starship.attack(targetStarship, 20);

        assertEquals(80, (int) starship.getEnergy());
    }

    @Test
    public void shouldConsumeEnergyWhileAbsorb() {
        starship.setEnergy(100);

        starship.absorbAttack(20);

        assertEquals(80, (int) starship.getEnergy());
    }

    @Test
    public void shouldNotBeDestroyedAfterMovingToEmptySector() {
        Galaxy galaxy = new Galaxy(8, 8);
        EnterpriseShip enterpriseShip = new EnterpriseShip(galaxy);
        enterpriseShip.placeInQuadrant(0, 1);
        enterpriseShip.placeInSector(0, 0);
        enterpriseShip.setEnergy(20);
        Star star = new Star(galaxy);
        star.placeInQuadrant(0, 1);
        star.placeInSector(1, 0);

        enterpriseShip.move(2, 0);

        assertFalse(enterpriseShip.destroyed());
    }

    @Test
    public void shouldBeDestroyedAfterMovingToSectorWithOtherObject() {
        Galaxy galaxy = new Galaxy(8, 8);
        EnterpriseShip enterpriseShip = new EnterpriseShip(galaxy);
        enterpriseShip.placeInQuadrant(0, 1);
        enterpriseShip.placeInSector(0, 0);
        Star star = new Star(galaxy);
        star.placeInQuadrant(0, 1);
        star.placeInSector(1, 0);

        enterpriseShip.move(1, 0);

        assertTrue(enterpriseShip.destroyed());
    }

    @Test
    public void shouldBeDestroyedAndRemovedFromMapAfterConsumingAllEnergy() {
        Galaxy galaxy = new Galaxy(8, 8);
        Starship starship = new Starship(galaxy);
        starship.placeInQuadrant(0, 0);
        starship.placeInSector(4, 4);
        starship.setEnergy(1);

        starship.consumeEnergy(2);

        assertTrue(starship.destroyed());
        assertNull(starship.getCurrentSector().getGameObject());
    }

    @Test
    public void shouldNotBeDestroyedIfEnergyWasNotSet() {
        Galaxy galaxy = new Galaxy(8, 8);
        Starship starship = new Starship(galaxy);
        starship.placeInQuadrant(0, 0);
        starship.placeInSector(4, 4);

        starship.consumeEnergy(2);

        assertFalse(starship.destroyed());
        assertNotNull(starship.getCurrentSector().getGameObject());
    }

    @Test
    public void shouldNotAllowMoveToNotExistingQuadrant() {
        Galaxy galaxy = new Galaxy(8, 8);
        Starship starship = new Starship(galaxy);
        starship.placeInQuadrant(0, 0);
        starship.placeInSector(0, 0);

        Quadrant quadrant = galaxy.getMapPartByCoordinates(0, 0);
        Sector sector = quadrant.getMapPartByCoordinates(0, 0);

        assertEquals(quadrant, starship.getCurrentQuadrant());
        assertEquals(sector, starship.getCurrentSector());
        assertFalse(starship.move(-1, -1));
    }

}
