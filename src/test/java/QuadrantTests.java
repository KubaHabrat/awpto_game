import map.Quadrant;
import map.Sector;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class QuadrantTests {

    @Test
    public void sectorsInQuadrantShouldBeInCorrectOrder() {

        Quadrant quadrant = new Quadrant(8, 8);

        Sector firstSector = quadrant.getChildMapParts().get(0);
        Sector lastSector = quadrant.getChildMapParts().get(quadrant.size() - 1);

        assertEquals(7, firstSector.getX());
        assertEquals(7, firstSector.getY());
        assertEquals(0, lastSector.getX());
        assertEquals(0, lastSector.getY());
    }
}
