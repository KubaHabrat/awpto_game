import map.Galaxy;
import map.Quadrant;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GalaxyTests {

    @Test
    public void quadrantsInGalaxyShouldBeInCorrectOrder() {
        Galaxy galaxy = new Galaxy(8, 8);

        Quadrant firstQuadrant = galaxy.getChildMapParts().get(0);
        Quadrant lastQuadrant = galaxy.getChildMapParts().get(galaxy.size() - 1);

        assertEquals(7, firstQuadrant.getX());
        assertEquals(7, firstQuadrant.getY());
        assertEquals(0, lastQuadrant.getX());
        assertEquals(0, lastQuadrant.getY());
    }
}
