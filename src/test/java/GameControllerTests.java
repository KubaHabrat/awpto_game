import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GameControllerTests {

    @Test
    public void shouldStartNewGame(){
        GameController gameController = new GameController();

        gameController.startNewGame();

        assertTrue(gameController.isGameRunning());
    }

    @Test
    public void shouldFinishGame(){
        GameController gameController = new GameController();

        gameController.startNewGame();
        gameController.finishGame();

        assertFalse(gameController.isGameRunning());
    }

    @Test
    public void shouldFinishGameWhenAllKlingonShipsAreDestroyed(){
        GameController gameController = new GameController();
        gameController.startNewGame();

        gameController.getGame().getKlingonShips().clear();
        gameController.checkGameStatus();

        assertFalse(gameController.isGameRunning());
        assertTrue(gameController.victory());
    }

    @Test
    public void shouldFinishGameWhenEnterpriseIsDestroyed(){
        GameController gameController = new GameController();
        gameController.startNewGame();

        gameController.getGame().getEnterpriseShip().destroy();
        gameController.checkGameStatus();

        assertFalse(gameController.isGameRunning());
        assertFalse(gameController.victory());
    }
}
