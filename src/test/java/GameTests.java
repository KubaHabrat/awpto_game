import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GameTests {

    @Test
    public void shouldHaveCreatedGalaxyAfterStart() {
        Game game = new Game();

        game.start();

        assertNotNull(game.getGalaxy());
    }

    @Test
    public void shouldHaveAllGameObjectsSpawnedAfterStart() {
        Game game = new Game();

        game.start();

        assertNotNull(game.getEnterpriseShip());
        assertEquals(7, game.getKlingonShips().size());
        assertEquals(20, game.getStars().size());
        assertEquals(2, game.getStarBases().size());
    }
}
