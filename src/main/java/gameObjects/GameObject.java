package gameObjects;

import map.Galaxy;
import map.Quadrant;
import map.Sector;

public class GameObject {

    Galaxy galaxy;
    Quadrant quadrant;
    Sector sector;

    public GameObject(Galaxy galaxy) {
        this.galaxy = galaxy;
    }

    public void placeInQuadrant(int x, int y) {
        this.quadrant = galaxy.getMapPartByCoordinates(x, y);
    }

    public void placeInSector(int x, int y) {
        Sector sector = this.quadrant.getMapPartByCoordinates(x, y);
        sector.placeObject(this);
        this.sector = sector;
    }
}
