package gameObjects;

import map.Galaxy;
import map.Quadrant;
import map.Sector;

import java.util.List;
import java.util.stream.Collectors;

public class Starship extends GameObject {

    int baseEnergy = -1;
    double energy;
    private boolean destroyed = false;
    private double starDates;

    public Starship(Galaxy galaxy) {
        super(galaxy);
    }

    public Quadrant getCurrentQuadrant() {
        return this.quadrant;
    }

    public Sector getCurrentSector() {
        return this.sector;
    }

    public double getEnergy() {
        return this.energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
        if (baseEnergy == -1) {
            this.baseEnergy = energy;
        }
    }

    public double getStarDates() {
        return this.starDates;
    }

    public void setStarDates(double starDates) {
        this.starDates = starDates;
    }

    public void consumeEnergy(int energyToConsume) {
        this.energy = this.energy - energyToConsume;
        if (this.baseEnergy != -1) {
            if (this.energy < 0) {
                destroy();
            }
        }
    }

    public void consumeStarDate(int starDatesToConsume) {
        this.starDates = this.starDates - starDatesToConsume;
    }

    public boolean destroyed() {
        return destroyed;
    }

    public boolean move(int xDistance, int yDistance) {
        boolean success = false;
        Quadrant currentQuadrant = getCurrentQuadrant();
        Sector currentSector = getCurrentSector();

        int energyCost = (int) calculateDistance(
                currentSector.getX(), currentSector.getX() + Math.abs(xDistance),
                currentSector.getY(), currentSector.getY() + Math.abs(yDistance));

        int xQuadrant = currentQuadrant.getX();
        int xSector = currentSector.getX();
        boolean changeQuadrant = false;
        while (xDistance > 0) {
            if (xSector == 7) {
                xQuadrant++;
                xSector = -1;
                changeQuadrant = true;
            }
            xSector++;
            xDistance--;
        }

        while (xDistance < 0) {
            if (xSector <= 0) {
                xQuadrant--;
                xSector = 8;
                changeQuadrant = true;
            }
            xSector--;
            xDistance++;
        }

        int yQuadrant = currentQuadrant.getY();
        int ySector = currentSector.getY();
        while (yDistance > 0) {
            if (ySector == 7) {
                yQuadrant++;
                ySector = -1;
                changeQuadrant = true;
            }
            ySector++;
            yDistance--;
        }

        while (yDistance < 0) {
            if (ySector <= 0) {
                yQuadrant--;
                ySector = 8;
                changeQuadrant = true;
            }
            ySector--;
            yDistance++;
        }

        Quadrant destinationQuadrant = galaxy.getMapPartByCoordinates(xQuadrant, yQuadrant);
        if (destinationQuadrant != null) {
            this.quadrant = destinationQuadrant;
            consumeEnergy(energyCost);
            currentSector.removeObject();
            if (changeQuadrant) {
                consumeStarDate((int) calculateDistance(
                        currentQuadrant.getX(), currentQuadrant.getX() + Math.abs(currentQuadrant.getX() - getCurrentQuadrant().getX()),
                        currentQuadrant.getY(), currentQuadrant.getY() + Math.abs(currentQuadrant.getY() - getCurrentQuadrant().getY())));
            }

            Sector destinationSector = this.quadrant.getMapPartByCoordinates(xSector, ySector);
            if (destinationSector.isNotEmpty()) {
                destroyed = true;
            }
            placeInSector(xSector, ySector);
            success = true;
        }
        return success;
    }

    public boolean attack(Starship target, int energy) {
        boolean success;
        double hitChance = (5 / (calculateDistance(getCurrentSector().getX(), target.getCurrentSector().getX(), getCurrentSector().getY(), target.getCurrentSector().getY()) + 4));
        consumeEnergy(energy);
        if (hitChance > Math.random()) {
            target.absorbAttack(energy);
            success = true;
        } else {
            success = false;
        }
        return success;
    }

    public void absorbAttack(int energy) {
        consumeEnergy(energy);
    }

    public void destroy() {
        destroyed = true;
        getCurrentSector().removeObject();
    }

    public <T extends Starship> T findTarget(Class<T> targetType) {
        List<T> availableTargets = getCurrentQuadrant()
                .getChildMapParts().stream()
                .filter(Sector::isNotEmpty)
                .filter(sector -> targetType.isInstance(sector.getGameObject()))
                .map(Sector::getGameObject)
                .map(targetType::cast)
                .collect(Collectors.toList());
        if (availableTargets.isEmpty()) {
            return null;
        }

        T closestTarget = availableTargets.get(0);
        double minDistance = calculateDistance(getCurrentSector().getX(), closestTarget.getCurrentSector().getX(), getCurrentSector().getY(), closestTarget.getCurrentSector().getY());
        for (T target : availableTargets) {
            double distance = calculateDistance(target.getCurrentSector().getX(), sector.getX(), target.getCurrentSector().getY(), sector.getY());
            if (minDistance > distance) {
                closestTarget = target;
                minDistance = distance;
            }
        }

        return closestTarget;
    }

    private double calculateDistance(double x1, double x2, double y1, double y2) {
        return Math.abs(x1 - x2) + Math.abs(y1 - y2);
    }
}
