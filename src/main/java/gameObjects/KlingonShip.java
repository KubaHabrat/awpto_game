package gameObjects;

import map.Galaxy;

public class KlingonShip extends Starship {
    public KlingonShip(Galaxy galaxy) {
        super(galaxy);
        setEnergy(100);
        baseEnergy = -1;
    }

    public String toString() {
        return "Klingon Ship";
    }
}
