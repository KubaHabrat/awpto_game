package gameObjects;

import map.Galaxy;
import map.Quadrant;
import map.Sector;

import java.util.ArrayList;
import java.util.List;

public class EnterpriseShip extends Starship {

    public EnterpriseShip(Galaxy galaxy) {
        super(galaxy);
    }

    public String getStatus() {
        return "Quadrant: " + getCurrentQuadrant().getX() + "," + getCurrentQuadrant().getY() + "\t" +
                "Sector: " + getCurrentSector().getX() + "," + getCurrentSector().getY() + "\t" +
                "Star dates: " + getStarDates() + "\t" +
                "Energy: " + getEnergy();
    }

    public boolean tryRefillEnergy() {
        boolean success = false;
        if (isStarBaseInNeighbourSector()) {
            this.energy = baseEnergy;
            success = true;
        }
        return success;
    }

    private boolean isStarBaseInNeighbourSector() {
        int currentSectorX = getCurrentSector().getX();
        int currentSectorY = getCurrentSector().getY();
        Quadrant currentQuadrant = getCurrentQuadrant();

        Sector right = currentQuadrant.getMapPartByCoordinates(currentSectorX, currentSectorY + 1);
        Sector left = currentQuadrant.getMapPartByCoordinates(currentSectorX, currentSectorY - 1);
        Sector lower = currentQuadrant.getMapPartByCoordinates(currentSectorX - 1, currentSectorY);
        Sector upper = currentQuadrant.getMapPartByCoordinates(currentSectorX + 1, currentSectorY);

        if (upper != null && upper.isNotEmpty()) {
            return upper.getGameObject() instanceof StarBase;
        } else if (lower != null && lower.isNotEmpty()) {
            return lower.getGameObject() instanceof StarBase;
        } else if (left != null && left.isNotEmpty()) {
            return left.getGameObject() instanceof StarBase;
        } else if (right != null && right.isNotEmpty()) {
            return right.getGameObject() instanceof StarBase;
        }
        return false;
    }

    public Quadrant shortScan() {
        return this.getCurrentQuadrant();
    }

    public List<Quadrant> longScan() {
        List<Quadrant> quadrants = new ArrayList<>();
        Quadrant currentQuadrant = getCurrentQuadrant();

        quadrants.add(galaxy.getMapPartByCoordinates(currentQuadrant.getX() - 1, currentQuadrant.getY() + 1));
        quadrants.add(galaxy.getMapPartByCoordinates(currentQuadrant.getX(), currentQuadrant.getY() + 1));
        quadrants.add(galaxy.getMapPartByCoordinates(currentQuadrant.getX() + 1, currentQuadrant.getY() + 1));
        quadrants.add(galaxy.getMapPartByCoordinates(currentQuadrant.getX() - 1, currentQuadrant.getY()));
        quadrants.add(currentQuadrant);
        quadrants.add(galaxy.getMapPartByCoordinates(currentQuadrant.getX() + 1, currentQuadrant.getY()));
        quadrants.add(galaxy.getMapPartByCoordinates(currentQuadrant.getX() - 1, currentQuadrant.getY() - 1));
        quadrants.add(galaxy.getMapPartByCoordinates(currentQuadrant.getX(), currentQuadrant.getY() - 1));
        quadrants.add(galaxy.getMapPartByCoordinates(currentQuadrant.getX() + 1, currentQuadrant.getY() - 1));
        return quadrants;
    }

    public String toString() {
        return "Enterprise ship";
    }
}
