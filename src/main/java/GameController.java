import gameObjects.EnterpriseShip;
import gameObjects.KlingonShip;

import java.util.InputMismatchException;

public class GameController {
    private final UserInterfaceUtils userInterfaceUtils = new UserInterfaceUtils();

    private Game game;
    private boolean victory;

    public void startNewGame() {
        this.game = new Game();
        this.game.start();
        userInterfaceUtils.printStartGame();
    }

    public Game getGame() {
        return game;
    }

    public boolean isGameRunning() {
        return this.game.isRunning();
    }

    public void checkEnterpriseStatus() {
        userInterfaceUtils.printEnterpriseStatus(this.game.getEnterpriseShip().getStatus());
    }

    public void finishGame() {
        this.game.finish();
        userInterfaceUtils.printEndGame();
    }

    public void readCommand() {
        int input = userInterfaceUtils.askForCommand();
        executeCommand(input);
    }

    public void initKlingonAttack() {
        game.getKlingonShips().forEach(klingonShip -> {
            EnterpriseShip enterpriseShip = klingonShip.findTarget(EnterpriseShip.class);
            if (enterpriseShip != null) {
                userInterfaceUtils.printKlingonAttack();
                int energy = 30;
                if (klingonShip.attack(enterpriseShip, energy)) {
                    userInterfaceUtils.printHit(enterpriseShip, energy);
                } else {
                    userInterfaceUtils.printMiss();
                }
            }
        });
    }

    public void checkGameStatus() {
        if (game.getEnterpriseShip().destroyed()) {
            game.finish();
            victory = false;
            userInterfaceUtils.printLost();
        }
        if (game.getKlingonShips().size() == 0) {
            game.finish();
            victory = true;
            userInterfaceUtils.printVictory();
        }
    }

    public boolean victory() {
        return victory;
    }

    public void tryRefillEnterprise() {
        if (game.getEnterpriseShip().tryRefillEnergy()) {
            userInterfaceUtils.printEnterpriseRefills();
        }
    }

    private void executeCommand(int command) {
        switch (command) {
            case 0:
                executeMove();
                break;
            case 1:
                executeShortScan();
                break;
            case 2:
                executeLongScan();
                break;
            case 3:
                executeEnterFightMode();
                break;
            case 4:
                executeShowGalaxyMap();
                break;
            default:
                throw new IllegalArgumentException("Command \"" + command + "\" is unsupported!");
        }
    }

    private void executeMove() {
        String vector = userInterfaceUtils.askForVectorToMove();
        int xDistance;
        int yDistance;
        try {
            xDistance = Integer.parseInt(vector.split(",")[0]);
            yDistance = Integer.parseInt(vector.split(",")[1]);
        } catch (Exception e) {
            throw new InputMismatchException();
        }
        if (!game.getEnterpriseShip().move(xDistance, yDistance)) {
            userInterfaceUtils.printQuadrantOutsideGalaxy();
        }

    }

    private void executeShortScan() {
        userInterfaceUtils.printSRScan(game.getEnterpriseShip().shortScan());
    }


    private void executeLongScan() {
        userInterfaceUtils.printLRScan(game.getEnterpriseShip().longScan());
    }

    private void executeEnterFightMode() {
        KlingonShip target = game.getEnterpriseShip().findTarget(KlingonShip.class);
        if (target != null) {
            userInterfaceUtils.printEnterAttackMode();
            checkEnterpriseStatus();
            int energy = userInterfaceUtils.askForEnergyToAttack();
            if (game.getEnterpriseShip().attack(target, energy)) {
                userInterfaceUtils.printHit(target, energy);
            } else {
                userInterfaceUtils.printMiss();
            }
            if (target.destroyed()) {
                game.getKlingonShips().remove(target);
                userInterfaceUtils.printTargetDestroyed(target);
            }
        } else {
            userInterfaceUtils.printNothingToAttack();
        }
    }

    private void executeShowGalaxyMap() {
        userInterfaceUtils.printGalaxyMap(game.getGalaxy());
    }


}
