import gameObjects.*;
import map.Galaxy;
import map.Quadrant;
import map.Sector;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Game {

    private static final int GALAXY_X_LENGTH = 8;
    private static final int GALAXY_Y_LENGTH = 8;
    private static final int QUADRANT_X_LENGTH = 8;
    private static final int QUADRANT_Y_LENGTH = 8;
    private static final int SECTOR_X_LENGTH = 8;
    private static final int SECTOR_Y_LENGTH = 8;
    private static final int KLINGON_SHIPS_AMOUNT = 7;
    private static final int STARS_AMOUNT = 20;
    private static final int STAR_BASES_AMOUNT = 2;

    private boolean running;
    private Galaxy galaxy;
    private EnterpriseShip enterpriseShip;
    private List<KlingonShip> klingonShips;
    private List<Star> stars;
    private List<StarBase> starBases;

    public void start() {
        this.galaxy = new Galaxy(GALAXY_X_LENGTH, GALAXY_Y_LENGTH);
        this.running = true;
        this.enterpriseShip = spawnEnterpriseShip();
        this.klingonShips = new ArrayList<>();
        this.starBases = new ArrayList<>();
        this.stars = new ArrayList<>();
        for (int i = 0; i < KLINGON_SHIPS_AMOUNT; i++) {
            this.klingonShips.add(createKlingonShip());
        }
        for (int i = 0; i < STARS_AMOUNT; i++) {
            this.stars.add(createStar());
        }
        for (int i = 0; i < STAR_BASES_AMOUNT; i++) {
            this.starBases.add(createStarBase());
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void finish() {
        this.running = false;
    }

    public Galaxy getGalaxy() {
        return this.galaxy;
    }

    public EnterpriseShip getEnterpriseShip() {
        return enterpriseShip;
    }

    public List<KlingonShip> getKlingonShips() {
        return this.klingonShips;
    }

    public List<Star> getStars() {
        return stars;
    }

    public List<StarBase> getStarBases() {
        return starBases;
    }

    private EnterpriseShip spawnEnterpriseShip() {
        EnterpriseShip enterpriseShip = new EnterpriseShip(getGalaxy());
        enterpriseShip.placeInQuadrant(4, 4);
        enterpriseShip.placeInSector(4, 4);
        enterpriseShip.setEnergy(600);
        enterpriseShip.setStarDates(60);
        return enterpriseShip;
    }

    private KlingonShip createKlingonShip() {
        KlingonShip klingonShip = new KlingonShip(getGalaxy());
        klingonShip.setEnergy(100);
        klingonShip.setStarDates(1);
        spawnObjectRandomlyInGalaxy(klingonShip);
        return klingonShip;
    }

    private Star createStar() {
        Star star = new Star(getGalaxy());
        spawnObjectRandomlyInGalaxy(star);
        return star;
    }

    private StarBase createStarBase() {
        StarBase starBase = new StarBase(getGalaxy());
        spawnObjectRandomlyInGalaxy(starBase);
        return starBase;
    }

    public void spawnObjectRandomlyInGalaxy(GameObject object) {
        Random random = new Random();

        int quadrantX = random.nextInt(QUADRANT_X_LENGTH);
        int quadrantY = random.nextInt(QUADRANT_Y_LENGTH);
        int sectorX = random.nextInt(SECTOR_X_LENGTH);
        int sectorY = random.nextInt(SECTOR_Y_LENGTH);

        Quadrant quadrant = this.galaxy.getMapPartByCoordinates(quadrantX, quadrantY);
        Sector sector = quadrant.getMapPartByCoordinates(sectorX, sectorY);

        while (sector.isNotEmpty()) {
            sectorX = random.nextInt(SECTOR_X_LENGTH);
            sectorY = random.nextInt(SECTOR_Y_LENGTH);
            sector = quadrant.getMapPartByCoordinates(sectorX, sectorY);
        }

        spawnObjectInGalaxy(object, quadrant, sector);
    }

    public void spawnObjectInGalaxy(GameObject object, Quadrant quadrant, Sector sector) {
        object.placeInQuadrant(quadrant.getX(), quadrant.getY());
        object.placeInSector(sector.getX(), sector.getY());
    }
}
