package map;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public abstract class ParentMapPart<T extends MapPart> {

    private final List<T> childMapParts = new ArrayList<>();

    public ParentMapPart(int xLength, int yLength, Supplier<T> supplier) {
        for (int x = xLength - 1; x >= 0; x--) {
            for (int y = yLength - 1; y >= 0; y--) {
                T mapPart = supplier.get();
                mapPart.setCoordinates(y, x);
                mapPart.setParent(this);
                childMapParts.add(mapPart);
            }
        }
    }

    public List<T> getChildMapParts() {
        return this.childMapParts;
    }

    public T getMapPartByCoordinates(int x, int y) {
        return this.childMapParts.stream()
                .filter(quadrant -> quadrant.getX() == x)
                .filter(quadrant -> quadrant.getY() == y)
                .findFirst()
                .orElse(null);
    }

    public int size() {
        return this.childMapParts.size();
    }
}
