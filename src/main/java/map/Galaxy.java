package map;

public class Galaxy extends ParentMapPart<Quadrant> {

    private static final int QUADRANT_X_LENGTH = 8;
    private static final int QUADRANT_Y_LENGTH = 8;

    public Galaxy(int xLength, int yLength) {
        super(xLength, yLength, () -> new Quadrant(QUADRANT_X_LENGTH, QUADRANT_Y_LENGTH));
    }
}
