package map;

import gameObjects.KlingonShip;
import gameObjects.Star;
import gameObjects.StarBase;

public class Quadrant extends ParentMapPart<Sector> implements MapPart {

    private int x;
    private int y;
    private Galaxy parentGalaxy;

    public Quadrant(int xLength, int yLength) {
        super(xLength, yLength, Sector::new);
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setCoordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void setParent(ParentMapPart parent) {
        this.parentGalaxy = (Galaxy) parent;
    }

    public Galaxy getParentGalaxy() {
        return parentGalaxy;
    }

    public String toString() {
        int starbases = (int) getChildMapParts().stream().filter(Sector::isNotEmpty).filter(sector -> sector.getGameObject() instanceof StarBase).count();
        int klingonsShips = (int) getChildMapParts().stream().filter(Sector::isNotEmpty).filter(sector -> sector.getGameObject() instanceof KlingonShip).count();
        int stars = (int) getChildMapParts().stream().filter(Sector::isNotEmpty).filter(sector -> sector.getGameObject() instanceof Star).count();
        return klingonsShips + "" + starbases + "" + stars + "\t";
    }

    public String getMap() {
        StringBuilder sb = new StringBuilder();
        for (int y = 7; y >= 0; y--) {
            for (int x = 0; x < 8; x++) {
                sb.append(getMapPartByCoordinates(x, y).toString()).append("\t");
            }
            sb.append("\n");
        }
        return sb.toString();
    }


}
