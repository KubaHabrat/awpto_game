package map;

import gameObjects.*;

public class Sector implements MapPart {

    private int x;
    private int y;
    private Quadrant parentQuadrant;
    private GameObject gameObject = null;

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    @Override
    public void setCoordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void setParent(ParentMapPart parent) {
        this.parentQuadrant = (Quadrant) parent;
    }

    public Quadrant getParentQuadrant() {
        return parentQuadrant;
    }

    public void placeObject(GameObject gameObject) {
        this.gameObject = gameObject;
    }

    public void removeObject() {
        this.gameObject = null;
    }

    public boolean isNotEmpty() {
        return gameObject != null;
    }

    public GameObject getGameObject() {
        return gameObject;
    }

    public String toString() {
        if (isNotEmpty()) {
            if (gameObject instanceof Star) {
                return " * ";
            } else if (gameObject instanceof KlingonShip) {
                return "+++";
            } else if (gameObject instanceof StarBase) {
                return ">!<";
            } else if (gameObject instanceof EnterpriseShip) {
                return "<*>";
            } else {
                return " ? ";
            }
        } else {
            return " - ";
        }
    }
}
