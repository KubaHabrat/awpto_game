package map;

public interface MapPart {
    void setCoordinates(int y, int x);

    void setParent(ParentMapPart parent);

    int getX();

    int getY();
}
