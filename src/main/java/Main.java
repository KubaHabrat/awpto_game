public class Main {
    public static void main(String[] args) {
        GameController gameController = new GameController();

        gameController.startNewGame();

        while (gameController.isGameRunning()) {
            gameController.tryRefillEnterprise();
            gameController.checkEnterpriseStatus();
            gameController.readCommand();

            gameController.initKlingonAttack();


            gameController.checkGameStatus();
        }
    }
}
