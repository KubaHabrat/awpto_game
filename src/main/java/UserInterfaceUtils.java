import gameObjects.KlingonShip;
import gameObjects.Starship;
import map.Galaxy;
import map.Quadrant;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class UserInterfaceUtils {

    Scanner scanner;

    public void printStartGame() {
        System.out.println("Starting new game...");
    }

    public void printEndGame() {
        System.out.println("The game has been ended...");
    }

    public void printEnterpriseStatus(String status) {
        System.out.println("ENTERPRISE STATUS:");
        System.out.println(status);
    }

    public int askForCommand() {
        System.out.println("ENTER COMMAND:");
        int input = -1;
        while (input == -1) {
            try {
                scanner = new Scanner(System.in);
                input = scanner.nextInt();
                if (input < 0 || input > 4) {
                    throw new InputMismatchException();
                }
            } catch (InputMismatchException e) {
                System.out.println("UNSUPPORTED COMMAND!");
                System.out.println("Valid commands are:");
                System.out.println("0 - Move\t 1 - Short range scan\t 2 - Long range scan\t 3 - Enter attack panel");
                input = -1;
            }
        }
        return input;
    }

    public String askForVectorToMove() {
        System.out.println("VECTOR?");
        String input = null;
        while (input == null) {
            scanner = new Scanner(System.in);
            input = scanner.nextLine();
            if (!input.matches("^-?\\d+,-?\\d+$")) {
                System.out.println("Incorrect vector format, please use following: X,Y");
                input = null;
            }
        }
        return input;
    }

    public void printSRScan(Quadrant quadrant) {
        System.out.println("SHORT RANGE SCAN:");

        System.out.println(quadrant.getMap());
    }

    public void printLRScan(List<Quadrant> quadrants) {
        System.out.println("SHORT RANGE SCAN:");
        int column = 1;
        for (Quadrant quadrant : quadrants) {
            if (quadrant == null) {
                System.out.print(" X " + "\t\t");
            } else {
                System.out.print(quadrant.toString() + "\t");
            }
            if (column % 3 == 0) {
                System.out.print("\n");
                column = 0;
            }
            column++;
        }
    }

    public void printEnterAttackMode() {
        System.out.println("FIGHT MODE:");
    }

    public int askForEnergyToAttack() {
        System.out.println("Enter number of energy to attack: ");
        scanner = new Scanner(System.in);
        int input = -1;
        while (input == -1) {
            try {
                scanner = new Scanner(System.in);
                input = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("UNSUPPORTED COMMAND!");
                System.out.println("Enter number of energy to Attack eg. '100'");
                input = -1;
            }
        }
        return input;
    }

    public void printNothingToAttack() {
        System.out.println("There is no target in current quadrant;");
    }

    public void printKlingonAttack() {
        System.out.println("Klingons attacked !!!");
    }

    public void printHit(Starship target, int energy) {
        System.out.println(energy + " energy absorbed by " + target.toString());
    }

    public void printMiss() {
        System.out.println("Miss...");
    }

    public void printGalaxyMap(Galaxy galaxy) {
        System.out.println("GALAXY MAP:");

        for (int y = 7; y >= 0; y--) {
            for (int x = 0; x < 8; x++) {
                Quadrant quadrant = galaxy.getMapPartByCoordinates(x, y);
                System.out.print("(" + quadrant.getX() + "," + quadrant.getY() + ") " + quadrant.toString() + "\t");
            }
            System.out.print("\n");
        }
    }

    public void printTargetDestroyed(Starship target) {
        if (target instanceof KlingonShip) {
            System.out.println(target.toString() + " has been destroyed!!!");
        }
    }

    public void printLost() {
        System.out.println("Enterprise has been destroyed!!!");
        System.out.println("You Lost...");
    }

    public void printVictory() {
        System.out.println("All Klingons ships has been destroyed!!!");
        System.out.println("You Won!!");
    }

    public void printEnterpriseRefills() {
        System.out.println("Enterprise energy has been refilled!!!");
    }

    public void printQuadrantOutsideGalaxy() {
        System.out.println("Can not navigate to indicated Quadrant because it is outside galaxy!!!");
    }
}
